﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bsa19.sr.linq.EntityData
{
    public class TeamData
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime CreatedAt { get; set; }
    }
}
