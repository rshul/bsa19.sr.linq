﻿using bsa19.sr.linq.EntityData;
using bsa19.sr.linq.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace bsa19.sr.linq
{
    public class DataRepository
    {
        public DataRepository()
        {
            HttpService.SetBaseUrl(@"https://bsa2019.azurewebsites.net/api/");
        }

        public async Task<IEnumerable<Project>> GetProjectsAsync()
        {
            return await HttpService.GetObjectsAsync<Project>("Projects");
        }

        public async Task<IEnumerable<ProjectTask>> GetTasksAsync()
        {
            return await HttpService.GetObjectsAsync<ProjectTask>("Tasks");
        }

        public async Task<IEnumerable<Team>> GetTeamsAsync()
        {
            return await HttpService.GetObjectsAsync<Team>("Teams");
        }

        public async Task<IEnumerable<User>> GetUsersAsync()
        {
            return await HttpService.GetObjectsAsync<User>("Users");
        }

        public async Task<IEnumerable<TaskState>> GetTaskStatusesAsync()
        {
            return await HttpService.GetObjectsAsync<TaskState>("TaskStates");
        }

        public async Task<IEnumerable<ProjectData>> GetAllProjectDataAsync()
        {
            IEnumerable<User> users = await GetUsersAsync();
            IEnumerable<Project> projects = await GetProjectsAsync();
            IEnumerable<ProjectTask> tasks = await GetTasksAsync();
            IEnumerable<Team> teams = await GetTeamsAsync();
            IEnumerable<TaskState> taskStates = await GetTaskStatusesAsync();
            

            var projectDatas = from p in projects
                               join u in users on p.Author_Id equals u.Id
                               join t in teams on p.Team_Id equals t.Id
                               join ta in tasks on p.Id equals ta.Project_Id into tGroup
                               from tg in tGroup
                               join us in users on tg.Performer_Id equals us.Id
                               select new ProjectData
                               {
                                   Id = p.Id,
                                   Author = new UserData
                                   {
                                       Id = u.Id,
                                       Birthday = u.Birthday,
                                       Email = u.Email,
                                       FirstName = u.First_name,
                                       LastName = u.Last_name,
                                       RegisteredAt = u.Registered_at,
                                       TeamId = u.Team_Id
                                   },
                                   Name = p.Name,
                                   CreatedAt = p.Created_At,
                                   Deadline = p.Deadline,
                                   Description = p.Description,
                                   TasksDatas = tGroup.Select(tg => new TaskData
                                   {
                                       Id = tg.Id,
                                       State = taskStates.Where(ts => ts.Id == tg.State).Select(ts => ts.Value).FirstOrDefault(),
                                       CreatedAt = tg.Created_At,
                                       Description = tg.Description,
                                       FinishedAt = tg.Finished_At,
                                       Name = tg.Name,
                                       Performer = new UserData
                                       {
                                           Id = us.Id,
                                           Birthday = us.Birthday,
                                           Email = us.Email,
                                           FirstName = us.First_name,
                                           LastName = us.Last_name,
                                           RegisteredAt = us.Registered_at,
                                           TeamId = us.Team_Id
                                       }
                                   }).ToList(),
                                   Team = new TeamData
                                   {
                                       Id = t.Id,
                                       Name = t.Name,
                                       CreatedAt = t.Created_At
                                   }
                               };

            return  await  Task.Run(() => projectDatas.ToList());
        }
    }
}
