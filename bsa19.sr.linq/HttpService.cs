﻿using System;
using Newtonsoft.Json;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Net.Http.Headers;

namespace bsa19.sr.linq
{
    public class HttpService
    {
        private static readonly HttpClient _client = new HttpClient();

        public static void SetBaseUrl(string url)
        {
            _client.BaseAddress = new Uri(url);
        }

        public static async Task<IEnumerable<T>> GetObjectsAsync<T>(string path)
        {
            IEnumerable<T> objects = null;
            HttpResponseMessage response = await _client.GetAsync(path);
            if (response.IsSuccessStatusCode)
            {
                
                objects = await response.Content.ReadAsAsync<IEnumerable<T>>();

            }
            return objects;
        }
    }
}
