﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bsa19.sr.linq.Models
{
    public class User
    {
        public int Id { get; set; }
        public string First_name { get; set; }
        public string Last_name { get; set; }
        public string Email { get; set; }
        public DateTime Birthday { get; set; }
        public DateTime Registered_at { get; set; }
        public int? Team_Id { get; set; }
    }
}
