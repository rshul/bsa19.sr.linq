﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bsa19.sr.linq.Models
{
    public class Team
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime Created_At { get; set; }
    }
}
