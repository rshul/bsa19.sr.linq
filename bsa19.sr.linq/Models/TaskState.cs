﻿using System;
using System.Collections.Generic;
using System.Text;

namespace bsa19.sr.linq.Models
{
    public class TaskState
    {
        public int Id { get; set; }
        public string Value { get; set; }
    }
}
